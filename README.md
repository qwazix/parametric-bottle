In the attached .blend, there is a bottle. You can create variations of it by adjusting it's shape keys, and choosing the corresponding UV maps to position the label correctly. You can mix & match the shape keys to create all kinds of combinations of shapes.

The shape keys control:

* The curvature of the upper part of the bottle
* The thickness of the neck
* Squareness of the body
* A recess for the label
* The cross section shape (rectangular - square), and
* Whether the cap region is flush or sticks out of the neck

You can either choose from the predefined materials or modify the nodes, which have been arranged into groups for easy editing.

Finally, the amount of booze is configurable by the shape keys of a transparent cube. Unfortunately I was not able to create the curvature of the liquid near the glass without breaking configurability.

This is licenced CC0 as I know it's hard to attribute every single prop in your scene, but I'd still love a scene if you got a credits screen.

## How to use

![screenshot](howtouse.png)

1. Click on the desired attribute shape key.
2. Change it's value. You can go back to step 1 to combine different values for the desired shape.
3. Some shapekeys deform the label. Click the camera near a UV map to choose the best mapping for your configuration. You can also unwrap the label yourself.
4. Change the color of the glass.
5. Change the label image texture in the material (a), or change the bottle material altogether: there are some presets available in the list of materials to get you started (b).
6. To control how much booze there is in the bottle, select the "Filling" cube (wire) and adjust it's shape key value.
7. To change the liquid color select the "Liquid" mesh (from the outliner or by right clicking twice on the bottle) and change the color of the Glass BSDF. Some examples are already stored as materials. 

To move the bottle, just move the parent mesh (glass). To copy it, select it along with it's children (Ctrl+G) and Duplicate (Shift+D)

